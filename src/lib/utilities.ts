import { exec } from 'child_process';

/**
 * Executes a command in child process.
 * @param command The command you want to execute.
 * @returns Returns the command output.
 */
 export function execAsync(command: string, cwd: string = null) {
  return new Promise<string>((resolve, reject) => {
    exec(command, {
      cwd: cwd ? cwd.trim() : process.cwd()
    }, (error, stdout, stderr) => {
      if (error) {
        reject(error.message);
        return;
      }

      if (stderr) {
        const lower = stderr.toLowerCase();
        if (
          !lower.startsWith('switched') &&
          !lower.startsWith('from http') &&
          !lower.startsWith('to http')
        ) {
          reject(stderr);
          return;
        }
      }

      resolve(stdout);
    });
  });
}